import os

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.template.defaultfilters import slugify
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm

from social_django.models import UserSocialAuth

from .models import CustomUser
from .forms import RegistrasiForm
from datetime import datetime

def registrasi(request):
    if not request.user.is_authenticated:
        registrasi_form = RegistrasiForm()

        if request.method == 'POST':
            data = request.POST.copy()
            data['username'] = slugify(data['name'])
            registrasi_form = RegistrasiForm(data=data)
            if registrasi_form.is_valid():
                try:
                    user = CustomUser.objects.create_user(
                        name=registrasi_form.cleaned_data['name'],
                        username=registrasi_form.cleaned_data['username'],
                        date_of_birth=registrasi_form.cleaned_data['date_of_birth'],
                        email=registrasi_form.cleaned_data['email'],
                        password=registrasi_form.cleaned_data['password'],
                    )
                    login(request, user, 'django.contrib.auth.backends.ModelBackend')
                    return render(request, 'registrasi/registrasi_sukses.html')
                except ValueError:
                    pass

        user_list = CustomUser.objects.all()
        response = {
            'registrasi_form': registrasi_form,
            'user_list': user_list
        }
        return render(request, 'registrasi/registrasi.html', response)
    else:
        return render(request, 'forbidden.html')

def login_view(request):
    if not request.user.is_authenticated:
        response = {}
        auth_form = AuthenticationForm()

        if request.method == 'POST':
            data = request.POST.copy()
            if not UserSocialAuth.objects.filter(uid=data['username']).exists():
                auth_form = AuthenticationForm(data=data)
                if auth_form.is_valid():
                    auth_form.clean()
                    user = auth_form.get_user()
                    if user is not None:
                        login(request, user, user.backend)
                        return render(request, 'registrasi/login_sukses.html', response)
                response['errors'] = "Login gagal. Pastikan email dan password anda benar."
            else:   # pragma: no cover
                # Tidak bisa test GoogleOAuth
                response['errors'] = "Anda mendaftar melalui Google, silahkan login melalui Google."
        
        response['auth_form'] = auth_form
        response['client_id'] = os.environ.get("SOCIAL_AUTH_GOOGLE_OAUTH2_KEY", 'noclientid')
        return render(request, 'registrasi/login.html', response)
    else:
        return render(request, 'forbidden.html')

def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect('landing-page:landing-page')
    else:
        return render(request, 'forbidden.html')
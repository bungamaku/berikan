from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.conf import settings
from django.contrib.auth.models import AnonymousUser

from unittest import mock

from registrasi.models import CustomUser
from registrasi.forms import RegistrasiForm
from registrasi.views import registrasi, login_view, logout_view

class RegistrasiModelsTestCase(TestCase):
    def test_custom_user_saved_correctly(self):
        user = CustomUser.objects.create_user(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')

        query = CustomUser.objects.filter(email='testUser@email.com', name='Test User')
        self.assertEqual(1, len(query))
        self.assertEqual(user, query[0])
        self.assertEqual(user.name, str(query[0]))
        self.assertEqual(user.name, query[0].get_full_name())
        self.assertEqual(user.name, query[0].get_short_name())

    def test_custom_user_create_superuser(self):
        user = CustomUser.objects.create_superuser(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')
        
        query = CustomUser.objects.filter(email='testUser@email.com', name='Test User')
        self.assertEqual(1, len(query))
        self.assertEqual(user, query[0])
        self.assertEqual(user.name, str(query[0]))
        self.assertEqual(user.name, query[0].get_full_name())
        self.assertEqual(user.name, query[0].get_short_name())

class RegistrasiFormsTestCase(TestCase):
    def test_registrasi_form_valid(self):
        data = {
            'name': 'Test User', 
            'username': 'Test-User', 
            'date_of_birth': '1999-12-04',
            'email': 'testUser@email.com',
            'password': 'passwordUser',
        }
        registrasi_form = RegistrasiForm(data=data)

        self.assertEqual(registrasi_form.is_valid(), True)
    
    def test_registrasi_form_invalid(self):
        data = {
            'name': '', 
            'username': '', 
            'date_of_birth': '',
            'email': 'testUser_error',
            'password': '',
        }
        registrasi_form = RegistrasiForm(data=data)

        self.assertEqual(registrasi_form.is_valid(), False)

class RegistrasiViewsTestCase(TestCase):
    def test_registrasi_url_exists(self):
        response = self.client.get(reverse('registrasi:registrasi'))
        self.assertEqual(response.status_code, 200)

    def test_registrasi_url_use_registrasi_view(self):
        registrasi_url = resolve(reverse('registrasi:registrasi'))
        self.assertEqual(registrasi_url.func, registrasi)

    def test_registrasi_view_render_correctly(self):
        registrasi_form = RegistrasiForm()

        response = self.client.get(reverse('registrasi:registrasi'))
        self.assertTemplateUsed(response, 'registrasi/registrasi.html')
        self.assertContains(response, registrasi_form['name'])
        self.assertContains(response, registrasi_form['email'])
        self.assertContains(response, registrasi_form['password'])
        
    def test_registrasi_view_post_success(self):
        data = {
            'name': 'Test User',
            'date_of_birth': '1999-12-04',
            'email': 'testEmail@email.com',
            'password': 'hahahihi123'
        }

        response = self.client.post(reverse('registrasi:registrasi'), data=data)

        registrasi_count = CustomUser.objects.all().count()
        self.assertEqual(registrasi_count, 1)

    def test_registrasi_view_post_failed(self):
        data = {
            'name': '',
            'date_of_birth': '',
            'email': '',
            'password': ''
        }

        response = self.client.post(reverse('registrasi:registrasi'), data=data)

        registrasi_count = CustomUser.objects.all().count()
        self.assertEqual(registrasi_count, 0)
    
    def test_login_url_exists(self):
        response = self.client.get(reverse('registrasi:login'))
        self.assertEqual(response.status_code, 200)

    def test_login_url_use_registrasi_view(self):
        login_url = resolve(reverse('registrasi:login'))
        self.assertEqual(login_url.func, login_view)

    def test_login_view_render_correctly(self):
        response = self.client.get(reverse('registrasi:login'))
        self.assertTemplateUsed(response, 'registrasi/login.html')
    
    def test_login_view_can_login(self):
        user = CustomUser.objects.create_user(email='testUser@email.com', username='testUser', 
                                              password='passwordUser', name='Test User', 
                                              date_of_birth='1999-12-04')

        data = {
            'username': 'testUser@email.com',
            'password': 'passwordUser'
        }
        response = self.client.post(reverse('registrasi:login'), data=data)
        user = response.context['user']

        self.assertEqual(user.email, 'testUser@email.com')
    
    def test_login_view_failed_login(self):
        user = CustomUser.objects.create_user(email='testUser@email.com', username='testUser', 
                                              password='passwordUser', name='Test User', 
                                              date_of_birth='1999-12-04')

        data = {
            'username': 'testUser@email.com',
            'password': 'hahahaha'
        }
        response = self.client.post(reverse('registrasi:login'), data=data)
        user = response.context['user']

        anon = AnonymousUser()
        self.assertEqual(user, anon)
        self.assertContains(response, "Login gagal. Pastikan email dan password anda benar.")
    
    def test_logout_view_success(self):
        user = CustomUser.objects.create_user(email='testUser@email.com', username='testUser', 
                                              password='passwordUser', name='Test User', 
                                              date_of_birth='1999-12-04')

        data = {
            'username': 'testUser@email.com',
            'password': 'passwordUser'
        }
        response = self.client.post(reverse('registrasi:login'), data=data)
        response_logout = self.client.get(reverse('registrasi:logout'), follow=True)
        user = response_logout.context['user']

        anon = AnonymousUser()
        self.assertEqual(user, anon)
        
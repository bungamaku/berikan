from django import forms
from datetime import datetime
from .models import CustomUser

year_range = 100
current_year = datetime.now().year

class RegistrasiForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['name', 'username', 'date_of_birth', 'email', 'password']
        labels = {
            'name': 'Nama',
            'username': 'Username',
            'date_of_birth': 'Tanggal Lahir',
            'email': 'Email',
            'password': 'Password',
        }
        widgets = {
            'name': forms.TextInput(attrs={'type': 'text', 'class': 'form-control form-input-registration'}),
            'username': forms.HiddenInput(),
            'date_of_birth': forms.SelectDateWidget(years=range(current_year - year_range, current_year), attrs={'type': 'date', 'class': 'form-control form-input-registration'}), 
            'email': forms.EmailInput(attrs={'type': 'email', 'class': 'form-control form-input-registration'}), 
            'password': forms.PasswordInput(attrs={'type': 'password', 'class': 'form-control form-input-registration'}),
        }
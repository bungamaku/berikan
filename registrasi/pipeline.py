def create_user(strategy, details, backend, user=None, *args, **kwargs):    # pragma: no cover
    # Bagian modifikasi dari pipeline social-auth-app-django
    USER_FIELDS = ['username', 'email']
    if user:
        return {'is_new': False}

    fields = dict((name, kwargs.get(name, details.get(name)))
                  for name in backend.setting('USER_FIELDS', USER_FIELDS))
    if backend.name == 'google-oauth2':
        fields['name'] = details.get('fullname')
    if not fields:
        return

    return {
        'is_new': True,
        'user': strategy.create_user(**fields)
    }

def set_extra_data(backend, strategy, details, response,
                   user=None, *args, **kwargs): # pragma: no cover
    # Bagian modifikasi dari pipeline social-auth-app-django
    if backend.name == 'google-oauth2':
        strategy.session_set('image_url', response['image'].get('url'))
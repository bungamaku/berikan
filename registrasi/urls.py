from django.urls import re_path
from .views import registrasi, login_view, logout_view

app_name = 'registrasi'

urlpatterns = [
	re_path(r'^registrasi$', registrasi, name = 'registrasi'),
	re_path(r'^login$', login_view, name = 'login'),
	re_path(r'^logout$', logout_view, name = 'logout'),
]

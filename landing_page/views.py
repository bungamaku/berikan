from django.shortcuts import render
from django.http import HttpResponse

def landing_page(request):
    page_title = "Mari Berikan!"
    response = {"title" : page_title}
    return render(request, 'landing_page/index.html', response)
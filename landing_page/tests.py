from django.test import TestCase
from django.test import Client

class ProgramUnitTest(TestCase):
    def test_main_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing_page/index.html')

    def test_landing_page_is_completed(self):
        response = Client().get('/')
        html_response = response.content.decode('UTF-8')
        self.assertIn('bantuan', html_response)
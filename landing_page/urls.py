from django.urls import re_path
from .views import landing_page

app_name = 'landing_page'

urlpatterns = [
    re_path(r'^$', landing_page, name='landing-page')
]
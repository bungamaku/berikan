from django.db import models
from django.conf import settings
from django.core.validators import MinValueValidator

from berita.models import Berita

class ProgramDonasi(models.Model):
    inisiator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='daftar_program_donasi')
    berita_terkait = models.ForeignKey(Berita, on_delete=models.SET_NULL, blank=True, null=True, related_name='daftar_program_donasi')
    judul = models.CharField(max_length=300)
    deskripsi = models.TextField()
    link_gambar = models.URLField(blank=True)
    target_donasi = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    tanggal_program = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.judul

    class Meta:
        verbose_name = 'Program Donasi'
        verbose_name_plural = 'Program Donasi'

class Donasi(models.Model):
    program_donasi = models.ForeignKey(ProgramDonasi, on_delete=models.CASCADE, related_name='daftar_donasi')
    donatur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='daftar_donasi')
    nama = models.CharField(max_length=300, default='Anonim')
    jumlah_donasi = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    anonim = models.BooleanField(default=True)
    tanggal_donasi = models.DateField(auto_now_add=True)

    def __str__(self):
        return 'Donasi ' + self.donatur.name + ' untuk ' + str(self.program_donasi)

    class Meta:
        verbose_name = 'Donasi'
        verbose_name_plural = 'Donasi'
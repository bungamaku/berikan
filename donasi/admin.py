from django.contrib import admin

from donasi.models import ProgramDonasi, Donasi


class ProgramDonasiAdminModel(admin.ModelAdmin):
    list_display = ('judul', 'inisiator', 'target_donasi',)

    class Meta:
        model = ProgramDonasi


class DonasiAdminModel(admin.ModelAdmin):
    list_display = ('program_donasi', 'donatur', 'nama', 'jumlah_donasi', 'anonim')

    class Meta:
        model = Donasi

admin.site.register(ProgramDonasi, ProgramDonasiAdminModel)
admin.site.register(Donasi, DonasiAdminModel)


from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model

from donasi.models import ProgramDonasi, Donasi
from donasi.forms import DonasiForm

def daftar_program_donasi(request):
    pd_list = ProgramDonasi.objects.all()
    for pd in pd_list:
        pd.target_donasi = format(pd.target_donasi, ",")

    response = {
        'pd_list': pd_list
    }

    return render(request, 'donasi/daftar_program_donasi.html', response)

def detail_program_donasi(request, pk):
    pd_detail = get_object_or_404(ProgramDonasi, pk=pk)
    pd_detail.target_donasi = format(pd_detail.target_donasi, ",")
    donasi_form = DonasiForm()
    error_akun = None

    if request.method == 'POST':
        data = request.POST.copy()
        data['program_donasi'] = pd_detail.id
        try:
            data['donatur'] = get_user_model().objects.get(email=data['email']).id
        except ObjectDoesNotExist:
            data['donatur'] = None
            error_akun = 'Akun dengan email tersebut tidak ditemukan!'
        donasi_form = DonasiForm(data=data)
        if donasi_form.is_valid():
            donasi_form.save()
            return render(request, 'donasi/donasi_sukses.html', {'pd_detail': pd_detail})

    response = {
        'pd_detail': pd_detail,
        'donasi_form': donasi_form,
        'error_akun': error_akun
    }
    return render(request, 'donasi/detail_program_donasi.html', response)

def daftar_donatur_program_donasi(request, pk):
    pd_detail = get_object_or_404(ProgramDonasi, pk=pk)
    pd_detail.target_donasi = format(pd_detail.target_donasi, ",")

    daftar_donasi = Donasi.objects.filter(program_donasi=pd_detail)
    banyak_donasi = Donasi.objects.filter(program_donasi=pd_detail).count()
    for donasi in daftar_donasi:
        donasi.jumlah_donasi = format(donasi.jumlah_donasi, ",")
        if donasi.anonim:
            donasi.nama = 'Anonim'

    if banyak_donasi == 0:
        banyak_donasi = 'Belum ada'

    response = {
        'pd_detail': pd_detail,
        'daftar_donasi': daftar_donasi,
        'banyak_donasi': banyak_donasi
    }
    return render(request, 'donasi/daftar_donatur_program_donasi.html', response)

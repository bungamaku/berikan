from django import forms
from donasi.models import Donasi

class DonasiForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'type': 'email', 'class': 'form-control'}))

    class Meta:
        model = Donasi
        fields = ['program_donasi', 'donatur', 'nama', 'email', 'jumlah_donasi', 'anonim']
        widgets = {
            'program_donasi': forms.Select(attrs={'class': 'form-control'}),
            'donatur': forms.Select(attrs={'class': 'form-control'}),
            'nama': forms.TextInput(attrs={'type': 'text', 'class': 'form-control'}),
            'jumlah_donasi': forms.NumberInput(attrs={'class': 'form-control'}),
            'anonim': forms.CheckboxInput(),
        }
        
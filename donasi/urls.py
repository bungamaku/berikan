from django.urls import re_path
from donasi.views import daftar_program_donasi, detail_program_donasi, daftar_donatur_program_donasi

app_name = 'donasi'

urlpatterns = [
    re_path(r'^program-donasi/$', daftar_program_donasi, name='daftar-program-donasi'),
    re_path(r'^program-donasi/(?P<pk>[0-9]+)/$', detail_program_donasi, name='detail-program-donasi'),
    re_path(r'^program-donasi/(?P<pk>[0-9]+)/donatur$', daftar_donatur_program_donasi, name='daftar-donatur-program-donasi'),
]
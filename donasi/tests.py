from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.conf import settings
from django.core.exceptions import FieldError
from django.contrib.auth import get_user_model

from unittest import mock

from donasi.models import ProgramDonasi, Donasi
from donasi.forms import DonasiForm
from donasi.views import daftar_program_donasi, detail_program_donasi, daftar_donatur_program_donasi

class DonasiModelsTestCase(TestCase):
    """
    This test case is intended for user alongside akun.CustomUser
    or any CustomUser that utilize email as username and include name field
    """
    def test_program_donasi_saved_correctly(self):
        user = get_user_model().objects.create_user(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')
        data = {
            'inisiator': user,
            'judul': 'Test Program Donasi',
            'deskripsi': 'Deskripsi Program Donasi',
            'link_gambar': 'https://www.sayastres.com',
            'target_donasi': 5000000
        }
        program_donasi = ProgramDonasi(**data)
        program_donasi.save()

        query = ProgramDonasi.objects.filter(**data)
        self.assertEqual(1, len(query))
        self.assertEqual(program_donasi, query[0])
        self.assertEqual(program_donasi.judul, str(query[0]))

    def test_donasi_saved_correctly(self):
        user = get_user_model().objects.create_user(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')
        pd_data = {
            'inisiator': user,
            'judul': 'Test Program Donasi',
            'deskripsi': 'Deskripsi Program Donasi',
            'link_gambar': 'https://www.sayastres.com',
            'target_donasi': 5000000
        }
        program_donasi = ProgramDonasi(**pd_data)
        program_donasi.save()

        data = {
            'program_donasi': program_donasi,
            'donatur': user,
            'jumlah_donasi': 50000,
            'anonim': True
        }
        donasi = Donasi(**data)
        donasi.save()

        query = Donasi.objects.filter(**data)
        self.assertEqual(1, len(query))
        self.assertEqual(donasi, query[0])
        self.assertEqual('Donasi ' + donasi.donatur.name + ' untuk ' + str(donasi.program_donasi), str(query[0]))

class DonasiFormsTestCase(TestCase):
    """
    This test case is intended for user alongside akun.CustomUser
    or any CustomUser that utilize email as username and include name field
    """
    def setUp(self):
        user = get_user_model().objects.create_user(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')
        
        pd_data = {
            'inisiator': user,
            'judul': 'Test Program Donasi',
            'deskripsi': 'Deskripsi Program Donasi',
            'link_gambar': 'https://www.sayastres.com',
            'target_donasi': 5000000
        }
        program_donasi = ProgramDonasi(**pd_data)
        program_donasi.save()

    def test_donasi_form_valid(self):
        user = get_user_model().objects.get(email='testUser@email.com')
        program_donasi = ProgramDonasi.objects.get(judul='Test Program Donasi')

        data = {
            'program_donasi': program_donasi.id,
            'donatur': user.id,
            'nama': 'Test User', 
            'email': 'testUser@email.com',
            'jumlah_donasi': 50000,
            'anonim': True
        }
        donasi_form = DonasiForm(data=data)

        self.assertEqual(donasi_form.is_valid(), True)
    
    def test_donasi_form_invalid(self):
        user = get_user_model().objects.get(email='testUser@email.com')
        program_donasi = ProgramDonasi.objects.get(judul='Test Program Donasi')

        data = {
            'program_donasi': None,
            'donatur': None,
            'nama': '', 
            'email': '',
            'jumlah_donasi': None,
            'anonim': True
        }
        donasi_form = DonasiForm(data=data)

        self.assertEqual(donasi_form.is_valid(), False)

class DonasiViewsTestCase(TestCase):
    """
    This test case is intended for user alongside akun.CustomUser
    or any CustomUser that utilize email as username and include name field
    """
    def setUp(self):
        user = get_user_model().objects.create_user(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')
        
        pd_data = {
            'inisiator': user,
            'judul': 'Test Program Donasi',
            'deskripsi': 'Deskripsi Program Donasi',
            'link_gambar': 'https://www.sayastres.com',
            'target_donasi': 5000000
        }
        program_donasi = ProgramDonasi(**pd_data)
        program_donasi.save()

    def test_daftar_program_donasi_url_exists(self):
        response = Client().get(reverse('donasi:daftar-program-donasi'))
        self.assertEqual(response.status_code, 200)

    def test_daftar_program_donasi_url_use_daftar_program_donasi_view(self):
        daftar_program_donasi_url = resolve(reverse('donasi:daftar-program-donasi'))
        self.assertEqual(daftar_program_donasi_url.func, daftar_program_donasi)

    def test_daftar_program_donasi_view_render_correctly(self):
        program_donasi = ProgramDonasi.objects.get(judul='Test Program Donasi')

        response = Client().get(reverse('donasi:daftar-program-donasi'))
        self.assertTemplateUsed(response, 'donasi/daftar_program_donasi.html')

    def test_detail_program_donasi_url_exists(self):
        response = Client().get(reverse('donasi:detail-program-donasi', args=[1]))
        self.assertEqual(response.status_code, 200)

    def test_detail_program_donasi_url_use_detail_program_donasi_view(self):
        detail_program_donasi_url = resolve(reverse('donasi:detail-program-donasi', args=[1]))
        self.assertEqual(detail_program_donasi_url.func, detail_program_donasi)

    def test_detail_program_donasi_view_render_correctly(self):
        program_donasi = ProgramDonasi.objects.get(judul='Test Program Donasi')
        donasi_form = DonasiForm()

        response = Client().get(reverse('donasi:detail-program-donasi', args=[1]))
        self.assertTemplateUsed(response, 'donasi/detail_program_donasi.html')
    
    def test_detail_program_donasi_view_post_success(self):
        user = get_user_model().objects.get(email='testUser@email.com')
        program_donasi = ProgramDonasi.objects.get(judul='Test Program Donasi')
        data = {
            'program_donasi': program_donasi.id,
            'donatur': user.id,
            'nama': 'Test User', 
            'email': 'testUser@email.com',
            'jumlah_donasi': 50000,
            'anonim': True
        }

        response = Client().post(reverse('donasi:detail-program-donasi', args=[1]), data=data)

        donasi_count = Donasi.objects.all().count()
        self.assertEqual(donasi_count, 1)

    def test_detail_program_donasi_view_post_failed(self):
        user = get_user_model().objects.get(email='testUser@email.com')
        program_donasi = ProgramDonasi.objects.get(judul='Test Program Donasi')
        data = {
            'program_donasi': None,
            'donatur': None,
            'nama': '', 
            'email': '',
            'jumlah_donasi': None,
            'anonim': True
        }

        response = Client().post(reverse('donasi:detail-program-donasi', args=[1]), data=data)

        donasi_count = Donasi.objects.all().count()
        self.assertEqual(donasi_count, 0)
    
    def test_detail_program_donasi_view_post_failed_donasi_negative(self):
        user = get_user_model().objects.get(email='testUser@email.com')
        program_donasi = ProgramDonasi.objects.get(judul='Test Program Donasi')
        data = {
            'program_donasi': None,
            'donatur': None,
            'nama': '', 
            'email': '',
            'jumlah_donasi': -1050,
            'anonim': True
        }

        response = Client().post(reverse('donasi:detail-program-donasi', args=[1]), data=data)

        donasi_count = Donasi.objects.all().count()
        self.assertEqual(donasi_count, 0)
    
    def test_daftar_donatur_program_donasi_url_exist(self):
        response = Client().get(reverse('donasi:daftar-donatur-program-donasi', args=[1]))
        self.assertEqual(response.status_code, 200)

    def test_daftar_donatur_program_donasi_use_correct_view_function(self):
        daftar_donatur_program_donasi_url = resolve(reverse('donasi:daftar-donatur-program-donasi', args=[1]))
        self.assertEqual(daftar_donatur_program_donasi_url.func, daftar_donatur_program_donasi)

    def test_daftar_donatur_program_donasi_render_correctly(self):
        user = get_user_model().objects.get(email='testUser@email.com')
        program_donasi = ProgramDonasi.objects.get(judul='Test Program Donasi')
        donasi_data = {
            'program_donasi': program_donasi,
            'donatur': user,
            'nama': user.name,
            'jumlah_donasi': 150000,
            'anonim': True
        }
        donasi = Donasi(**donasi_data)
        donasi.save()

        donasi_data_2 = {
            'program_donasi': program_donasi,
            'donatur': user,
            'nama': 'Aku ga mau ditampilin',
            'jumlah_donasi': 150000,
            'anonim': False
        }
        donasi_2 = Donasi(**donasi_data_2)
        donasi_2.save

        daftar_donasi = Donasi.objects.filter(program_donasi=program_donasi)

        response = Client().get(reverse('donasi:daftar-donatur-program-donasi', args=[1]))
        self.assertTemplateUsed(response, 'donasi/daftar_donatur_program_donasi.html')
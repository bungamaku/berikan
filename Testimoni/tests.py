from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import indexs
from .models import testimoniModel

class Testimoni_Test(TestCase):
    def test_if_url_and_template_are_exist(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'testimoni.html')

    def test_if_function_for_html_rendering_exists(self):
        found = resolve('/testimoni/')
        self.assertEqual(found.func,indexs)

    def test_form_testimoni(self):
        pass

    def test_success_insert_testimoni(self):
        pass

    def test_cant_access_without_session(self):
        pass

from django.shortcuts import render,redirect
from .models import testimoniModel
from django.http import JsonResponse
# Create your views here.

def indexs(request):
    buatkonten = testimoniModel.objects.all()
    response = {
        'initial': buatkonten
    }
    return render(request, 'testimoni.html', response)

def testimoni(request):
    nama = request.GET.get('namaTestimoni', None)
    isi  = request.GET.get('testimoni', None)
    validate_desc = not (len(isi) == 0 or isi.isspace())

    #Jika sudah terautentikasi dan tervalidasi(sudah login), model membuat modelnya
    if request.user.is_authenticated and validate_desc:
        testimoniModel.objects.create(namaTestimoni=nama, testimoni=isi)

        buatkonten = testimoniModel.objects.all()
        response = []
        for i in buatkonten :
            response.append({
                'namaTestimoni' : i.namaTestimoni,
                'testimoni'  : i.testimoni
            }) 

        return JsonResponse(response, safe=False)

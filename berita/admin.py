from django.contrib import admin
from berita.models import Berita

class BeritaAdminModel(admin.ModelAdmin):
    list_display = ('judul', 'deskripsi', 'link_gambar')

    class Meta:
        model = Berita

admin.site.register(Berita, BeritaAdminModel)

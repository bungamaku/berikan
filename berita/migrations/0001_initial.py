# Generated by Django 2.1.1 on 2018-12-02 10:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Berita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('judul', models.CharField(max_length=300)),
                ('deskripsi', models.TextField()),
                ('link_gambar', models.URLField(blank=True)),
            ],
            options={
                'verbose_name': 'Berita',
                'verbose_name_plural': 'Berita',
            },
        ),
    ]

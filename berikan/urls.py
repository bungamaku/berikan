from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import re_path, include, path

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^auth/', include('social_django.urls', namespace='social_auth')),
    re_path(r'^', include('landing_page.urls', namespace='landing-page')),
    re_path(r'^', include('registrasi.urls', namespace='registrasi')),
    re_path(r'^', include('donasi.urls', namespace='donasi')),
    re_path(r'^', include('berita.urls', namespace='berita')),
    path('testimoni/',include(('Testimoni.urls','Testimoni'),namespace='Testimoni')),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Tugas 1 PPW 2018/2019
## Informasi Deployments
[![pipeline status](https://gitlab.com/bungamaku/berikan/badges/master/pipeline.svg)](https://gitlab.com/bungamaku/berikan/commits/master)

[![coverage report](https://gitlab.com/bungamaku/berikan/badges/master/coverage.svg)](https://gitlab.com/bungamaku/berikan/commits/master)

[Link HerokuApp](https://berikan.herokuapp.com/)

## Informasi Anggota Kelompok
Kelas : PPW-E
Kelompok : 11
Anggota :

1. 1406568721 - Alif Karnadi Y

2. 1706022104 - Bunga Amalia Kurniawati

3. 1706074676 - Ananda Daffa Sabila Isliyana

4. 1706028663 - Pande Ketut Cahya Nugraha (Pindahan dari kelompok lain) 

## Informasi Pembagian Tugas Pada Tugas Kelompok 1
Pada tugas ini, kami memilih story Crowd Funding. Crown Funding adalah sistem berbasis web untuk melakukan penggalangan dana yang secara beramai-ramai digalang dari masyarakat. Untuk merealisasikan story tersebut, kami menciptakan web bernama Berikan.

Dalam web Berikan, terdapat beberapa fitur yaitu :
-   Formulir pendaftaran jadi donatur 
	- Penanggung Jawab : Ananda Daffa
	- Nama Aplikasi dan Branch : registrasi
-   Formulir submit donasi
    - Penanggung Jawab : Alif Karnadi
	- Nama Aplikasi dan Branch : donasi
-   Halaman yang menampilkan daftar donasi per program
	- Penanggung Jawab : Bunga Amalia
	- Nama Aplikasi dan Branch : program
-   Halaman pembuka
	- Penanggung Jawab : Bunga Amalia
	- Nama Aplikasi dan Branch : landing_page

Wireframe dan prototype dibuat oleh Bunga Amalia.

## Informasi Pembagian Tugas Pada Tugas Kelompok 2
Terdapat perombakan pada aplikasi, dimana aplikasi donasi dan program digabungkan kedalam aplikasi donasi

Dalam web Berikan, terdapat beberapa fitur tambahan yaitu :
- Login  with Google
	- Penanggung Jawab : Pande Ketut Cahya Nugraha
- Daftar List Program dan Donasi ke suatu program (bagi yang sudah login)
	- Penanggung Jawab : Bunga Amalia
- Daftar Donasi yang pernah dilakukan (bagi yang sudah login)
	- Penanggung Jawab : Ananda Daffa
- Halaman About dan Testimoni untuk halaman about
	 - Penanggung Jawab : Alif Karnadi


## Informasi Branch
1. Pastikan requirements sudah terinstall dan env sudah diaktifkan.
2. Sejajar dengan folder env, buatlah folder bernama "berikan".
3. Di dalam folder "berikan", lakukan `git clone https://gitlab.com/bungamaku/berikan.git`
4. Setiap kali sebelum ingin melakukan perubahan, jangan lupa untuk meng-update local repository dengan melakukan `git pull origin master`
5. Buatlah branch baru untuk setiap app dengan melakukan `git checkout -b [nama_app]`
6. Jika branch untuk app tersebut sudah ada, maka lakukan `git checkout [nama_app]`
7. Ketika ingin menge-push perubahan, lakukan `git push origin [nama_app]`
8. Jika sudah cukup melakukan perubahan dan ingin menggabungkannya ke branch master, maka tambahkan merge request baru.

## Informasi Static Files
1. Files static yang akan digunakan oleh semua app diletakkan di common_files/static.
2. Syntax untuk penggunaan common static: `{% static 'styles/nama_folder/nama_file' %}`
3. Templates yang akan digunakan oleh semua app diletakkan common_files/templates.
4. Syntax untuk penggunaan common templates: `nama_file`
5. Static yang hanya digunakan oleh satu app diletakkan di nama_app/static/nama_app. 
6. Syntax untuk penggunaan static app `{% static 'nama_app/styles/nama_folder/nama_file %}`
7. Templates yang hanya digunakan oleh satu app diletakkan di nama_app/templates/nama_app. 
8. Syntax untuk penggunaan templates app `'nama_app/nama_file'`